import Link from "next/link";
import Head from "next/head";

import { useEffect } from "react";
import useRouter from "next/router";
const NotFound = () => {
  const router = useRouter;

  useEffect(() => {
    setTimeout(() => {
      router.push("/");
    }, 3000);
  }, [router]);
  return (
    <>
      <Head>
        <title>
          <title>Next app || Not found</title>
        </title>
      </Head>
      <div className="not-found">
        <h1>oopss...</h1>
        <h2>Page is not found!</h2>
        <Link href={"/"}>
          <a> Go to home page</a>
        </Link>
      </div>
    </>
  );
};

export default NotFound;
