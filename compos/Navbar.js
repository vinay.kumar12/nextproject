import Link from "next/link";
import Image from "next/image";

const Navbar = () => {
  return (
    <nav>
      <div className="logo">
        <Image src="/logo.jpg" width={100} height={60} />
      </div>
      <Link href={"/"}>
        <a>Home</a>
      </Link>
      <Link href={"/About"}>
        <a>About</a>
      </Link>
      <Link href={"/vinay/Test"}>
        <a>Next app info</a>
      </Link>
    </nav>
  );
};

export default Navbar;
